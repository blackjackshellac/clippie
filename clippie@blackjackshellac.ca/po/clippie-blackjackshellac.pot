# Clippie
# Copyright (C) 2021, Steeve McCauley
# This file is distributed under the same license as the clippie-blackjackshellac package.
# Steeve McCauley <steeve.mccauley@gmail.com>, 2001
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: clippie-blackjackshellac\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-16 14:09-0400\n"
"PO-Revision-Date: 2021-04-16 14:09-0400\n"
"Last-Translator: Steeve McCauley <steeve.mccauley@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: indicator.js:38
msgid "Clippie"
msgstr ""

#: menus.js:67 menus.js:68 menus.js:73 menus.js:91 menus.js:96
msgid "More…"
msgstr ""

#: menus.js:260 menus.js:263 menus.js:299 menus.js:304
msgid "Search"
msgstr ""

#: schemas/org.gnome.shell.extensions.clippie-blackjackshellac.gschema.xml:26
msgid "Turn on debugging"
msgstr ""

#: schemas/org.gnome.shell.extensions.clippie-blackjackshellac.gschema.xml:30
msgid "Maximum number of clipboard entries to show on the main menu"
msgstr ""

#: schemas/org.gnome.shell.extensions.clippie-blackjackshellac.gschema.xml:34
msgid "Information to restore state after restart"
msgstr ""
